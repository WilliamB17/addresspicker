class CreateLocalisation < ActiveRecord::Migration[5.0]
  def change
    create_table :localisations do |t|
      t.integer "number", null: false
      t.string "street", null: false
      t.string "postal_code", limit: 5, null: false
      t.string "city", null: false
      t.string "country", null: false
    end
  end
end
