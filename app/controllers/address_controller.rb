class AddressController < ApplicationController
  protect_from_forgery with: :exception

  def index
    localisations = Localisation.all
    if params[:order] == 'ASC'
      localisations.order!('postal_code ASC')
    elsif params[:order] == 'DESC'
      localisations.order!('postal_code DESC')
    end
    render :index, locals:{localisations: localisations}
  end

  def create
    localisation = Localisation.new(address_params)
    unless localisation.valid?
      flash[:danger] = "Non valide !"
    else
      localisation.save
      flash[:info] = "Enregistré avec succès !"
    end
    redirect_to root_path
  end

  def new

  end

  protected
  def address_params
    params.permit(
        :number,
        :street,
        :city,
        :postal_code,
        :country
    )
  end
end
