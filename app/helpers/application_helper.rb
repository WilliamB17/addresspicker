module ApplicationHelper

  def flash_class(type)
    case type
      when "info" then "alert alert-info"
      when "success" then "alert alert-success"
      when "warning" then "alert alert-warning"
      when "danger" then "alert alert-danger"
      else "alert alert-danger"
    end
  end

end
