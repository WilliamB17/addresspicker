// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


//= require bootstrap-sprockets
//= require bloodhound.min.js
//= require typeahead.jquery.min.js
//= require typeahead-addresspicker.min.js



document.addEventListener("turbolinks:load", function() {
    addressPicker()
})

function addressPicker(){
    var addressPicker = new AddressPicker(
        {
            autocompleteService: {
                componentRestrictions: { country: 'FR' }
            }
        }
    );

    $('#address').typeahead(
        {hint: false, highlight: false},
        {
            displayKey: 'description',
            source: addressPicker.ttAdapter()
        }
    );

    addressPicker.bindDefaultTypeaheadEvent($('#address'))

    $(addressPicker).on('addresspicker:selected', function (event, result) {
        $('#city').val(result.nameForType('locality'));
        $('#number').val(result.nameForType('street_number'));
        $('#street').val(result.nameForType('route'));
        $('#country').val(result.nameForType('country', true));
        $('#postal_code').val(result.nameForType('postal_code'));
    });
}

