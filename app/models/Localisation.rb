class Localisation < ActiveRecord::Base

  validates :number, :street, :city, :postal_code, :country, presence: true

end
